FROM golang:alpine as builder

LABEL maintainer="Ondřej Tůma <sirTurbis@gmail.com>"
WORKDIR /build

COPY source/ .
RUN apk --no-cache add ca-certificates git
RUN go get -d -v
RUN CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' -o exporter
WORKDIR /dist
RUN cp /build/exporter .

FROM scratch

LABEL maintainer="Ondřej Tůma <sirTurbis@gmail.com>"

ENV OPSGENIE_API=""
ENV OPSGENIE_KEY=""
ENV NOTIFICATOR_API=""
ENV NOTIFICATOR_KEY=""
ENV NOTIFICATOR_INBOXES=""

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /dist/exporter /

ENTRYPOINT ["/exporter"]

EXPOSE 9000
