package main

type EnumItem struct {
	index int
	name  string
}

type Enum struct {
	items []EnumItem
}

func (enum Enum) Index(findName string) int {
	for idx, item := range enum.items {
		if findName == item.name {
			return idx
		}
	}
	return -1
}
