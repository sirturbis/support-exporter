package main

import (
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var addr = ":9000"
var opsgenieAPI = os.Getenv("OPSGENIE_API")
var opsgenieToken = os.Getenv("OPSGENIE_KEY")
var notificatorAPI = os.Getenv("NOTIFICATOR_API")
var notificatorToken = os.Getenv("NOTIFICATOR_KEY")
var notificatorInbox = os.Getenv("NOTIFICATOR_INBOXES")

//flag.String("notificator-inboxes", "Affilbox,DOS,Emeldi,FAPI,Fine Tech,Lnenicka,LUKAPO @ Info,LUKAPO @ Servery,MioWeb,Olympic,Quitec,Robeeto,Shipmonk,SmartEmailing @ Servery,Statusoid @ Servers,SmartSelling @ Servery", "List of inboxes which will be scrapped")

func main() {

	opsgenie := newOpsgenieCollector(opsgenieAPI, opsgenieToken)
	notificator := newnotificatorCollector(notificatorAPI, notificatorToken, strings.Split(notificatorInbox, ","))

	prometheus.MustRegister(opsgenie)
	prometheus.MustRegister(notificator)

	http.Handle("/", promhttp.Handler())
	http.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(addr, nil))
}
