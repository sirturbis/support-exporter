package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
)

type opsgenieCollector struct {
	metric      *prometheus.Desc
	metricCount *prometheus.Desc
	opsgenieAPI string
	opsgenieKEY string
}

type OpsGenieAlerts struct {
	Data []struct {
		Acknowledged bool   `json:"acknowledged"`
		Alias        string `json:"alias"`
		Count        int64  `json:"count"`
		CreatedAt    string `json:"createdAt"`
		ID           string `json:"id"`
		Integration  struct {
			ID   string `json:"id"`
			Name string `json:"name"`
			Type string `json:"type"`
		} `json:"integration"`
		IsSeen         bool   `json:"isSeen"`
		LastOccurredAt string `json:"lastOccurredAt"`
		Message        string `json:"message"`
		Owner          string `json:"owner"`
		OwnerTeamID    string `json:"ownerTeamId"`
		Priority       string `json:"priority"`
		Report         struct {
			AckTime        int64  `json:"ackTime"`
			AcknowledgedBy string `json:"acknowledgedBy"`
		} `json:"report"`
		Responders []struct {
			ID   string `json:"id"`
			Type string `json:"type"`
		} `json:"responders"`
		Seen         bool     `json:"seen"`
		Snoozed      bool     `json:"snoozed"`
		SnoozedUntil string   `json:"snoozedUntil"`
		Source       string   `json:"source"`
		Status       string   `json:"status"`
		Tags         []string `json:"tags"`
		Teams        []struct {
			ID string `json:"id"`
		} `json:"teams"`
		TinyID    string `json:"tinyId"`
		UpdatedAt string `json:"updatedAt"`
	} `json:"data"`
	Paging struct {
		First string `json:"first"`
		Last  string `json:"last"`
	} `json:"paging"`
	RequestID string  `json:"requestId"`
	Took      float64 `json:"took"`
}

var AlertStates = Enum{[]EnumItem{{0, "open"}, {1, "acknowledged"}, {2, "snoozed"}}}

func newOpsgenieCollector(api string, key string) *opsgenieCollector {
	return &opsgenieCollector{
		metric: prometheus.NewDesc("opsgenie_alerts",
			"Alerts from OpsGenie",
			[]string{
				"status",
				"source",
				"message",
				"createAt",
			}, nil,
		),
		metricCount: prometheus.NewDesc("opsgenie_alerts_count",
			"Count of alerts with same type",
			[]string{
				"type",
			}, nil,
		),
		opsgenieAPI: api,
		opsgenieKEY: key,
	}
}

func (collector *opsgenieCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- collector.metric
	ch <- collector.metricCount
}

func (collector *opsgenieCollector) Collect(ch chan<- prometheus.Metric) {

	url := collector.opsgenieAPI + "/alerts?query=status%3Aopen"
	bearer := "GenieKey " + collector.opsgenieKEY
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", bearer)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error on response.\n[ERROR] -", err)
	}
	alerts := OpsGenieAlerts{}
	json.NewDecoder(resp.Body).Decode(&alerts)

	var openCount float64 = 0.0
	var ackCount float64 = 0.0
	var snoozedCount float64 = 0.0

	for number := range alerts.Data {
		alert := alerts.Data[number]
		var statusCode int
		if alert.Snoozed == true {
			statusCode = AlertStates.Index("snoozed")
			snoozedCount++
		} else if alert.Acknowledged == true {
			statusCode = AlertStates.Index("acknowledged")
			ackCount++
		} else {
			statusCode = AlertStates.Index("open")
			openCount++
		}
		ch <- prometheus.MustNewConstMetric(collector.metric, prometheus.GaugeValue, 1.0, strconv.Itoa(statusCode), alert.Source, alert.Message, alert.CreatedAt)
	}
	ch <- prometheus.MustNewConstMetric(collector.metricCount, prometheus.GaugeValue, snoozedCount, "snoozed")
	ch <- prometheus.MustNewConstMetric(collector.metricCount, prometheus.GaugeValue, ackCount, "acknowledged")
	ch <- prometheus.MustNewConstMetric(collector.metricCount, prometheus.GaugeValue, openCount, "open")
}
