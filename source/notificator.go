package main

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
)

type notificatorCollector struct {
	metric         *prometheus.Desc
	metricCount    *prometheus.Desc
	notificatorAPI string
	notificatorKEY string
	inboxes        []string
}

type notificatorTickets struct {
	Next    string `json:"next"`
	Page    int    `json:"page"`
	Results []struct {
		InboxName string `json:"inbox_name"`
		Subject   string `json:"subject"`
		TicketID  int    `json:"ticket_id"`
	} `json:"results"`
}

func newnotificatorCollector(api string, key string, inboxes []string) *notificatorCollector {
	return &notificatorCollector{
		metric: prometheus.NewDesc("notificator_tickets",
			"Tickets from notificator",
			[]string{
				"ID",
				"inboxName",
				"subject",
			}, nil,
		),
		metricCount: prometheus.NewDesc("notificator_tickets_count",
			"Count of tickets by inbox",
			[]string{
				"inboxName",
			}, nil,
		),
		notificatorAPI: api,
		notificatorKEY: key,
		inboxes:        inboxes,
	}
}

func (collector *notificatorCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- collector.metric
	ch <- collector.metricCount
}

func (collector *notificatorCollector) Collect(ch chan<- prometheus.Metric) {
	for inbox := range collector.inboxes {
		url := collector.notificatorAPI + "/fetch/twd"
		bearer := collector.notificatorKEY
		bodyStart := []byte(`{"filter":{"inbox_name":"`)
		bodyInbox := []byte(collector.inboxes[inbox])
		bodyEnd := []byte(`","agent_id": null,"status":"active"},"mask": ["ticket_id","inbox_name","subject"],"method": "fetch"}`)
		body := [][]byte{bodyStart, bodyInbox, bodyEnd}
		req, err := http.NewRequest("POST", url, bytes.NewBuffer(bytes.Join(body, []byte(""))))
		req.Header.Add("x-user-token", bearer)
		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("accept", "application/json")
		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			log.Println("Error on response.\n[ERROR] -", err)
		}
		tickets := notificatorTickets{}
		json.NewDecoder(resp.Body).Decode(&tickets)
		for number := range tickets.Results {
			ticket := tickets.Results[number]
			ch <- prometheus.MustNewConstMetric(collector.metric, prometheus.GaugeValue, 1.0, strconv.Itoa(ticket.TicketID), ticket.InboxName, ticket.Subject)

		}
		ch <- prometheus.MustNewConstMetric(collector.metricCount, prometheus.GaugeValue, float64(len(tickets.Results)), collector.inboxes[inbox])
	}
}
